// Libraries require
const express = require('express')
const app = express()
const logger = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')

// Controller requires
const userRouter = require('./controllers/userController')
const loginRouter = require('./controllers/loginController')

// Middlewear requires
const auth = require('./middlewear/auth')

// Misc. requires
const config = require('./utils/config')
const productRouter = require('./controllers/productController')

// Execution Start
const mongoUrl = config.MONGODB_URI

console.log('Connecting to mongodb')

mongoose
    .connect(mongoUrl)
    .then((res) => {
        console.log('Connected to mongodb')
    })
    .catch((error) => {
        console.log('Error connecting to mongodb: ', error.message)
    })

// Middlewear use
app.use(logger('dev'))
app.use(cors())
app.use(express.json())

// Routes that don't need auth
app.use('/api/users', userRouter)
app.use('/api/login', loginRouter)

app.use(auth)

// Routes that need auth
app.use('/api/products', productRouter)

app.use('*', (request, response) => {
    response.json({
        message: "Not a valid API endpoint"
    })
})

module.exports = app