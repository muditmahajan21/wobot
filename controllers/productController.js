const productRouter = require('express').Router()
const Product = require('../models/product')

productRouter.get('/', async (request, response) => {
    try {
        const products = await Product.find({})
        response.json(products.map(product => product.toJSON()))
    } catch (error) {
        console.log(error)
        response.status(500).json({
            error: 'Server error'
        })
    }
})

productRouter.post('/', async (request, response) => {

})

module.exports = productRouter