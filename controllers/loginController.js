const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const loginRouter = require('express').Router()
const User = require('../models/user')

loginRouter.post('/', async (request, response) => {
    try {
        const {username, password} = request.body

        const currUser = await User.findOne({username})

        const passwordCorrect = currUser === null ? false : await bcrypt.compare(password, currUser.passwordHash)

        if(!(currUser && passwordCorrect)) {
            return response.status(401).json({
                error: 'Invalid username or password'
            })
        }

        const userForToken = {
            username: currUser.username,
            id: currUser._id,
        }

        const token = jwt.sign(userForToken, process.env.SECRET, {expiresIn: '1h'})

        currUser.token = token
        currUser.save()
        response.status(200).send({
            token, 
            username: currUser.username,
        })
    } catch (error) {
        console.log(error)
        response.status(500).json({
            error: 'Server error'
        })
    }
})

module.exports = loginRouter