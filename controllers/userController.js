const bcrypt = require('bcrypt')
const userRouter = require('express').Router()
const User = require('../models/user')
const jwt = require('jsonwebtoken')

userRouter.get('/', async (request, response) => {
    try {
        const users = await User.find({})
        response.json(users.map(user => user.toJSON()))
    } catch (error) {
        console.log(error)
        response.status(500).json({
            error: 'Server error'
        })
    }
})

userRouter.get('/:id', async (request, response) => {
    try {
        const user = await User.findById(request.params.id)
        response.json(user.toJSON())
    } catch (error) {
        console.log(error)
        response.status(500).json({
            error: 'Server error'
        })
    }
})

userRouter.post('/', async (request, response) => {
    try {
        const {firstName, lastName, username, password} = request.body

        if(!firstName || !lastName || !username || !password) {
            return response.status(400).json({
                error: 'Missing fields'
            })
        }

        const existingUser = await User.findOne({username: username})
        
        if(existingUser) {
            return response.status(400).json({
                error: 'Username already taken'
            })
        }

        const saltRounds = 10
        const passwordHash = await bcrypt.hash(password, saltRounds)

        const user = await User.create({
            firstName,
            lastName,
            username,
            passwordHash
        })

        user.save()

        response.status(201).json(user.toJSON())
    } catch (error) {
        console.log(error)
        response.status(500).json({
            error: 'Server error'
        })
    }
})

userRouter.delete('/:id', async(request, response) => {
    try {
        await User.findByIdAndRemove(request.params.id)
        response.status(204).end() 
    } catch (error) {
        console.log(error)
        response.status(500).json({
            error: 'Server error'
        })
    }
})

module.exports = userRouter